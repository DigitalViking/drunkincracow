﻿
using System;
using System.IO;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrunkInCracow.Code;

namespace DrunkInCracow.Tests
{
   [TestClass]
   public class UnitTestGoogleApiCache
   {
      [TestMethod]
      public void CreateDatabase()
      {
         GoogleApiCache.On = true;
         string dbfile = ConfigurationManager.AppSettings["DataFolder"] + "cache.db";
         GoogleApiCache.Init(dbfile);
         bool rval = File.Exists(dbfile);
         Assert.IsTrue(rval);
         /* cleanup */
         File.Delete(dbfile);
      }

      [TestMethod]
      public void SaveBarInfo_Save_Jeson_Doc()
      {
         string key = "ChIJ8Tb4__8PFkcRwHm1b4IG3Zc";
         string file = ConfigurationManager.AppSettings["JsonFilesFolder"] + "BarDetailsReduced.json";
         string jdoc = File.ReadAllText(file).Trim();
         string dbfile = ConfigurationManager.AppSettings["DataFolder"] + "cache.db";
         GoogleApiCache.On = true;
         GoogleApiCache.Init(dbfile);
         bool rval = GoogleApiCache.SaveBarInfo(key, jdoc);
         Assert.IsTrue(rval);
         /* cleanup */
         File.Delete(dbfile);
      }

      [TestMethod]
      public void ReadBarInfo_Read_Jeson_Doc()
      {
         Tuple<bool, string> rval = this.PrepDatabase();
         string key = "ChIJ8Tb4__8PFkcRwHm1b4IG3Zc";
         string dbfile = ConfigurationManager.AppSettings["DataFolder"] + "cache.db";
         GoogleApiCache.On = true;
         GoogleApiCache.Init(dbfile);
         string buff = GoogleApiCache.ReadBarInfo(key);
         // ---
         string hex_a = rval.Item2.ComputeMD5Hash();
         string hex_b = buff.ComputeMD5Hash();
         Assert.AreEqual(hex_b, hex_a);
         /* cleanup */
         File.Delete(dbfile);
      }

      private Tuple<bool,string> PrepDatabase()
      {
         string key = "ChIJ8Tb4__8PFkcRwHm1b4IG3Zc";
         string file = ConfigurationManager.AppSettings["JsonFilesFolder"] + "BarDetailsReduced.json";
         string jdoc = File.ReadAllText(file).Trim();
         string dbfile = ConfigurationManager.AppSettings["DataFolder"] + "cache.db";
         GoogleApiCache.On = true;
         GoogleApiCache.Init(dbfile);
         bool rvl = GoogleApiCache.SaveBarInfo(key, jdoc);
         return Tuple.Create(rvl, jdoc);
      }
   }
}
