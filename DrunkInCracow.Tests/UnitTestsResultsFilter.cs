﻿
using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;
using DrunkInCracow.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DrunkInCracow.Tests
{
   [TestClass]
   public class UnitTestsResultsFilter
   {
      [TestMethod]
      public void BarListReduce_Read_Json_Google_Format_Outpu_New_Format()
      {
         string jdir = ConfigurationManager.AppSettings["JsonFilesFolder"];
         string file = jdir + "BarList.json";
         string buff = File.ReadAllText(file).Trim();
         buff = ResultsFilter.BarListReduce(buff);
         string json_in_hash = "D9-CD-C8-37-5F-C9-A6-E8-D6-68-F3-A7-16-41-21-D1";
         string md5 = buff.ComputeMD5Hash();
         Assert.AreEqual(json_in_hash, md5);
      }

      [TestMethod]
      public void BarInfoReduce_Read_Json_Google_Format_Outpu_New_Format()
      {
         
         string jdir = ConfigurationManager.AppSettings["JsonFilesFolder"];
         string file = jdir + "BarDetails.json";
         string buff = File.ReadAllText(file).Trim();
         buff = ResultsFilter.BarInfoReduce(buff);
         string json_in_hash = "A4-2E-44-34-5F-88-FD-AD-B6-23-22-B6-2F-FB-B0-4C";
         string md5 = buff.ComputeMD5Hash();
         Assert.AreEqual(json_in_hash, md5);
      }
   }
}
