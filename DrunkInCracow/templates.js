﻿
var Templs = {
    BasicBarInfo: "<div><div id=\"{0}\" class=\"basic-info\" title=\"Click for more info.\"><div>{1}</div><div>{2}</div><div>{3}</div><div>{4}</div></div>" +
	    "<div class=\"detail-info\"></div></div>",
    DetailBarInfo: "",
    OpenHours: "<fieldset><legend>Operating Hours</legend>{0}</fieldset>",
    OpenHoursItem: "<div class=\"open-hrs-item\"><div>{0}</div><div>{1}</div></div>",
    TelWebsite:"<div class=\"tel-website\"><div>{0}</div><div><a href=\"{1}\">Bar Website</a></div>" + 
        "<div><a href=\"{2}\">View Map</a></div></div>",
    AdPlace: "<div class=\"ad-placeholder\"></div>",
    SearchBox:"<div class=\"search-box\"><div><input type=\"text\" id=\"txtSearchBox\"/>" +
        "<input type=\"button\" id=\"btnSearch\" value=\"Search\"/></div><script type=\"text/javascript\">" +
        "$(\"#btnSearch\").click(App.Search);</script></div>",
    SearchingMsg: "<div class=\"searching-box\"><div></div></div>",
    MsgBox: "<div class=\"msg-box\">{0}<div></div></div>"
};
