﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DrunkInCracow.Code;

namespace DrunkInCracow.Controllers
{
   public class BarInfoController : ApiController
   {
      [HttpGet]
      public string GetBarInfo(string placeid)
      {
         string rbuff = null;
         try
         {
            GoogleApi googleApi = new GoogleApi();
            rbuff = googleApi.GetBarInfo(placeid);
         }
         catch (Exception exp)
         {
            Logger.Write(exp);
            return AppResponse.Error(1);
         }
         // ---
         return AppResponse.Success(rbuff);
         // ---
      }

   }
}
