﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DrunkInCracow.Code;

namespace DrunkInCracow.Controllers
{
   public class BarsController : ApiController
   {
      [HttpGet]
      public string GetBars()
      {
         string rbuff = null;
         try
         {
            GoogleApi googleApi = new GoogleApi();
            rbuff = googleApi.GetBarList();
         }catch(Exception exp)
         {
            Logger.Write(exp);
            return AppResponse.Error(1);
         }
         // ---
         return AppResponse.Success(rbuff);
         // ---
      }

      [HttpPost]
      public string SearchBars()
      {
        
         string rbuff = null;
         try
         {
            string kw = HttpContext.Current.Request.Form["Keyword"];
            GoogleApi googleApi = new GoogleApi();
            rbuff = googleApi.GetBarList(kw);
         }
         catch (Exception exp)
         {
            Logger.Write(exp);
            return AppResponse.Error(1);
         }
         // ---
         return AppResponse.Success(rbuff);
         // ---
      }
   }
}
