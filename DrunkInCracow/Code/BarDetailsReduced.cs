﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DrunkInCracow.Code
{
   public class BarDetailsReduced
   {
      public readonly string[] OpenHours;
      public readonly string Website;
      public readonly string TelNum;
      public readonly string MapUrl;

      public BarDetailsReduced(CracowBarInfo cbi)
      {
         this.OpenHours = (cbi.result.opening_hours == null) ? new string[] { } : cbi.result.opening_hours.weekday_text;
         this.TelNum = cbi.result.formatted_phone_number;
         this.MapUrl = cbi.result.url;
         this.Website = cbi.result.website;
      }
   }
}