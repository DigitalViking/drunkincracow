﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Http;

namespace DrunkInCracow.Code
{
   public class OtherApi
   {

      public string GetCityTemp()
      {
         string url = @"http://courseware.infomatrix.us/syscalls/?SysCall=ReadLocalTemp";
         return this.RunQuery(url);
      }

      public string RunQuery(string url)
      {
         // ---
         string buff;
         WebResponse res = null;
         StreamReader sreader = null;
         // ---
         try
         {
            res = ((HttpWebRequest)WebRequest.Create(url)).GetResponse();
            sreader = new StreamReader(res.GetResponseStream());
            buff = sreader.ReadToEnd();
         }
         finally
         {
            Trash.Dispose(res, sreader);
         }
         // ---
         return buff;
         // ---
      }
   }
}