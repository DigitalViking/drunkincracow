﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DrunkInCracow.Code
{
   public class Trash
   {
      public static void Dispose(params IDisposable[] args)
      {
         foreach(IDisposable dis in args)
         {
            if (dis != null)
               dis.Dispose();
         }
      }
   }
}