﻿
using System.IO;
using System.Configuration;
using System.Net;

namespace DrunkInCracow.Code
{
   public class GoogleApi
   {

      private string apiKey = null;

      public GoogleApi()
      {
         this.apiKey = ConfigurationManager.AppSettings["ApiKey"];
      }

      public string GetBarList(string keyword = "")
      {
         string url = ConfigurationManager.AppSettings["BarListUrl"];
         string loc = ConfigurationManager.AppSettings["CenterLoc"];
         string rad = ConfigurationManager.AppSettings["CenterLocRadius"];
         url = url.FormatEx(new[] { loc, rad, keyword, this.apiKey });
         string buff = this.RunQuery(url);
         buff = ResultsFilter.BarListReduce(buff);
         return buff;
      }

      public string GetBarInfo(string placeid)
      {
         // ---
         string jdoc = null, url = null;
         if (GoogleApiCache.On)
            jdoc = GoogleApiCache.ReadBarInfo(placeid);
         // ---
         if (string.IsNullOrEmpty(jdoc))
         {
            url = ConfigurationManager.AppSettings["BarInfoUrl"];
            url = url.FormatEx(new[] { placeid, this.apiKey });
            jdoc = ResultsFilter.BarInfoReduce(this.RunQuery(url));
            if (GoogleApiCache.On)
               GoogleApiCache.SaveBarInfo(placeid, jdoc);
         }
         return jdoc;
      }

      private string RunQuery(string url)
      {
         // ---
         string buff;
         WebResponse res = null;
         StreamReader sreader = null;
         // ---
         try
         {
            res = ((HttpWebRequest)WebRequest.Create(url)).GetResponse();
            sreader = new StreamReader(res.GetResponseStream());
            buff = sreader.ReadToEnd();
         }
         finally
         {
            Trash.Dispose(res, sreader);
         }
         // ---
         return buff;
         // ---
      }
   }
}