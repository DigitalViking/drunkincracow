﻿
using System;

namespace DrunkInCracow.Code
{
   public class BarInfoReduced
   {
      public string Name;
      public string Address;
      public string PlaceID;
      public float? Rating;
      public string IsOpenNow;
      public BarInfoReduced(Result r)
      {
         this.Name = r.name;
         this.Address = r.vicinity;
         this.PlaceID = r.place_id;
         this.Rating = r.rating;
         this.IsOpenNow = (r.opening_hours == null) ? "?" : Convert.ToInt32(r.opening_hours.open_now).ToString();
      }
   }
}