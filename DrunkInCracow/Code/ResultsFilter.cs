﻿
using System.Linq;
using Newtonsoft.Json;

namespace DrunkInCracow.Code
{
   public class ResultsFilter
   {
      public static string BarListReduce(string buff)
      {
         CracowBarsResult rset = JsonConvert.DeserializeObject<CracowBarsResult>(buff);
         var lst = rset.results.Select(r => new BarInfoReduced(r));
         JsonSerializerSettings settings = new JsonSerializerSettings();
         settings.NullValueHandling = NullValueHandling.Include;
         return JsonConvert.SerializeObject(lst, settings);
      }

      public static string BarInfoReduce(string buff)
      {
         CracowBarInfo cbi = JsonConvert.DeserializeObject<CracowBarInfo>(buff);
         return JsonConvert.SerializeObject(new BarDetailsReduced(cbi));
      }
   }
}