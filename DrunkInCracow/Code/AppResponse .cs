﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DrunkInCracow.Code
{
   public class AppResponse
   {
      private static string ResponseTemplate = "{{\"ErrorCode\":\"{0}\", \"ErrorMsg\":\"{1}\", \"Payload\":{2}}}";
      public static string Success(string payload)
      {
         return AppResponse.ResponseTemplate.FormatEx(0, "OK", payload);
      }

      public static string Error(int error, string msg = null)
      {
         if (msg == null)
            msg = "Lookup error msg in the system by ErrorCode";
         return AppResponse.ResponseTemplate.FormatEx(error, msg, "");
      }
   }
}