﻿
using System;
using System.Text;
using System.Security.Cryptography;

namespace DrunkInCracow.Code
{
   public static class Extensions
   {
      public static string FormatEx(this string str, params object[] args)
      {
         return String.Format(str, args);
      }

      public static string ComputeMD5Hash(this string str)
      {
         byte[] bytes = Encoding.ASCII.GetBytes(str);
         byte[] hash = MD5.Create().ComputeHash(bytes);
         return BitConverter.ToString(hash);
      }
   }
}
