﻿
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data;
using System.Data.SQLite;

namespace DrunkInCracow.Code
{
   public class GoogleApiCache
   {

      public static bool On = false;
      private static string dbFilePath = null;
      private static string dbConnString = null;
      private static SQLiteConnection dbConn = null;

      static GoogleApiCache()
      { }

      public static void Init(string dbfile)
      {
         dbFilePath = dbfile;
         if (!File.Exists(dbFilePath))
            CreateCache();
         dbConnString = "DataSource={0};".FormatEx(dbFilePath);
         if (dbConn == null)
            dbConn = new SQLiteConnection(dbConnString);
      }

      public static string ReadBarInfo(string placeid)
      {
         string sql = "select JsonDoc from BarDetailInfo where PlaceID = '{0}';".FormatEx(placeid);
         SQLiteCommand cmd = new SQLiteCommand(sql, dbConn);
         if(cmd.Connection.State != ConnectionState.Open)
            cmd.Connection.Open();
         object jdoc = cmd.ExecuteScalar(CommandBehavior.CloseConnection);
         Trash.Dispose(cmd);
         return (string)jdoc;
      }

      public static bool SaveBarInfo(string key, string jdoc)
      {
         string sql = "insert into BarDetailInfo values('{0}', datetime('now'), '{1}');".FormatEx(key, jdoc);
         SQLiteCommand cmd = new SQLiteCommand(sql, dbConn);
         if (cmd.Connection.State != ConnectionState.Open)
            cmd.Connection.Open();
         int rcount = cmd.ExecuteNonQuery(CommandBehavior.CloseConnection);
         Trash.Dispose(cmd);
         return (rcount == 1);
      }

      private static void CreateCache()
      {
         string connstr = "DataSource={0};".FormatEx(dbFilePath);
         string sql = "CREATE TABLE `BarDetailInfo` (`PlaceID` TEXT UNIQUE, `dts` TEXT, `JsonDoc` TEXT)";
         SQLiteConnection.CreateFile(dbFilePath);
         SQLiteConnection dbconn = new SQLiteConnection(connstr);
         SQLiteCommand cmd = new SQLiteCommand(sql, dbconn);
         cmd.Connection.Open();
         cmd.ExecuteNonQuery();
         dbconn.Close();
         Trash.Dispose(cmd, dbconn);
      }
   }
}