﻿
// + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
var ViewPort = {
    ID:"#AppViewPort",
    Clear: function () {
        $(ViewPort.ID).html("");
    },
    Append: function (html) {
        $(ViewPort.ID).html(html);
    },
    Html:function(html){
        $(ViewPort.ID).html(html);
    },
    ClearBG: function () {
        $(ViewPort.ID).css("background-image", "none");
    },
    Css: function (key, val) {
        $(ViewPort.ID).css(key, val);
    },
    DisplaySearching: function () {
        this.Clear();
        this.Append(Templs.SearchingMsg);
    }
}

// + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + + +
var App = {
    // ---
    BarsUrl: "/api/bars/",
    BarInfo: "/api/barinfo/{0}",
    CityTempUrl:"/SysCalls/RPC.aspx?Name=GetCityTemp",
    // ---
    Init: function () {
        if (!"".Format)
            alert("extends.js file was not loaded");
        $("#AppViewPort").html(Templs.SearchBox);
        $("#btnSearchTop").click(App.Search);
    },
    // ---
    LoadBarInfo: function () {
        // ---
        var __this__ = this;
        if ($(this).next().html() === "") {
            var url = App.BarInfo.Format([this.id]);
            $.get(url, function (jstr) {
                var obj = JSON.parse(jstr);
                if (parseInt(obj.ErrorCode) === 0) {
                    App.DisplayBarInfo(obj, __this__);
                } else {
                    App.DisplayError(obj);
                }
            });
        } else {
            App.ToggleBarDetails($(this).next());
        }
        // ---
    },
    // ---
    LoadBars: function (keyword) {
        if (keyword === undefined) {
            $.get(App.BarsUrl, function (jstr) {
                var obj = JSON.parse(jstr);
                if (parseInt(obj.ErrorCode) === 0) {
                    App.DisplayBarList(obj);
                } else {
                    App.DisplayError(obj);
                }
            });
        } else {
            $.post(App.BarsUrl, {"Keyword":keyword}, function (jstr) {
                var obj = JSON.parse(jstr);
                if (parseInt(obj.ErrorCode) === 0) {
                    App.DisplayBarList(obj);
                } else {
                    App.DisplayError(obj);
                }
            });
        }
    },
    // ---
    DisplayBarList: function (obj) {
        // ---
        var arr = null, html = null, name = null, door = null;
        var door = { "?": "We don't know", "0": "Now Closed", "1": "Now Opened" };
        // ---
        ViewPort.Clear();
        if ($(obj.Payload).length == 0) {
            console.log("X: " + obj.Payload);
            var msg = Templs.MsgBox.Format(["Nothing Found..."]);
            ViewPort.Html(msg);
            return false;
        }
        // ---
        $(obj.Payload).each(function (idx, item) {
            name = (item.Name.length > 40) ? item.Name.substr(0, 40) + "..." : item.Name;
            arr = [item.PlaceID, name, item.Rating.toFixed(1), "", door[item.IsOpenNow]];
            html = Templs.BasicBarInfo.Format(arr);
            $("#AppViewPort").append(html);
        });
        $(".basic-info").click(App.LoadBarInfo);
    },
    // ---
    DisplayError: function (obj) {
        console.log(obj.ErrorMsg);
    },
    // ---
    DisplayBarInfo: function (obj, __this__) {
        var buff = "";
        var __next__ = $(__this__).next();
        App.ToggleBarDetails(__next__);
        // working hrs
        $(obj.Payload.OpenHours).each(function (idx, item) {
            buff += Templs.OpenHoursItem.Format(App.SplitOpenHrs(item));
        });
        var html = Templs.OpenHours.Format([buff]);
        $(__next__).append(html);
        // tel & website
        var tel = (obj.Payload.TelNum == "") ? "Not Avaliable" : obj.Payload.TelNum;
        var lnk = (obj.Payload.Website == "") ? "Not Avaliable" : obj.Payload.Website;
        var map = (obj.Payload.MapUrl == "") ? "Not Avaliable" : obj.Payload.MapUrl;
        html = Templs.TelWebsite.Format([tel, lnk, map]);
        $(__next__).append(html);
        $(__next__).append(Templs.AdPlace);
    },
    // ---
    ToggleBarDetails:function(__next__){
        var display = $(__next__).css("display");
        display = (display === "none") ? "block" : "none";
        $(__next__).css("display", display);
    },
    // ---
    SplitOpenHrs: function (buff) {
        var pos = buff.indexOf(":");
        var day = buff.substring(0, ++pos);
        var hrs = buff.substring(pos);
        return [day, hrs];
    },
    // ---
    Search: function () {
        // ---
        var txt = null;
        if (this.id == "btnSearchTop")
            txt = $("#txtSearchBoxTop").val();
        else
            txt = $("#txtSearchBox").val();
        txt = (txt == "") ? undefined : txt;
        $("#TopSearchBox").css("display", "block");
        ViewPort.ClearBG();
        ViewPort.Css("margin-top", "8px");
        ViewPort.Css("height", "auto");
        ViewPort.DisplaySearching();
        $("#CityTempDiv").css("display", "none");
        // ---
        App.LoadBars(txt);
        // ---
    },
    // ---
    GetCityTemp: function () {
        $.get(App.CityTempUrl, function (txt) {
            var jobj = JSON.parse(txt);
            $("#CityTempDiv").html("Your are in: " + jobj.City + " - Local tempature is: " + jobj.Temp + "&deg;C");
            $("#CityTempTop").html(jobj.City + "  :  " + jobj.Temp + "&deg;C");
        });
    }
};
