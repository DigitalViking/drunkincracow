﻿
using System;
using System.Web;
using DrunkInCracow.Code;

namespace DrunkInCracow.SysCalls
{
   public partial class RPC : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         OtherApi oapi = new OtherApi();
         string buff = oapi.GetCityTemp();
         Response.Write(buff);        
      }
   }
}