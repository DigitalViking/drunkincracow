﻿
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using DrunkInCracow.Code;

namespace DrunkInCracow
{
   public class WebApiApplication : System.Web.HttpApplication
   {
      protected void Application_Start()
      {
         GlobalConfiguration.Configure(WebApiConfig.Register);
         string dbfile = HttpContext.Current.Server.MapPath("~/App_Data/cache.db");
         GoogleApiCache.On = true;
         GoogleApiCache.Init(dbfile);
      }
   }
}
